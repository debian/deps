# This file is part of the DEPS/graph-includes package
#
# (c) 2005,2006 Yann Dirson <ydirson@altern.org>
# Distributed under version 2 of the GNU GPL.

package DEPS::Node;
use strict;
use warnings;

use base qw(DEPS::Object);
use Carp qw(croak);
use Set::Object qw();

# Objects keys of this class are intentionally not locked, since many
# attributes will be used for various purposes, and it probably makes
# no sense to make things more complicated.

sub new {
  my $class = shift;
  my $self = {};

  ($self->{LABEL}) = @_;

  # sanity check
  croak "node name must not be an object or reference" if ref $self->{LABEL};

  bless ($self, $class);
  return $self;
}

1;
