# This file is part of the DEPS/graph-includes package
#
# (c) 2006 Yann Dirson <ydirson@altern.org>
# Distributed under version 2 of the GNU GPL.

package DEPS::Style;

use warnings;
use strict;

use Hash::Util qw(lock_keys);
use Carp qw(croak);

sub new {
  my $class = shift;
  my $required = shift;
  my $optional = shift;
  my %args = @_;
  my $self = {};

  # import %args
  foreach my $key (keys %args) {
    if (grep { $key eq $_ } @$required, @$optional) {
      $self->{$key} = $args{$key};
    } else {
      croak "${class}::new does not understand attribute '$key'";
    }
  }

  # sanity check
  foreach my $key (@$required) {
    croak "no value provided for '$key'" unless defined $self->{$key};
  }

  bless ($self, $class);
  lock_keys (%$self);
  return $self;
}

1;
