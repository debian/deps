# This file is part of the DEPS/graph-includes package
#
# (c) 2005,2006 Yann Dirson <ydirson@altern.org>
# Distributed under version 2 of the GNU GPL.

package graphincludes::params;
use strict;
use warnings;

our $VERSION='0.13';
our $verbose=0;
our $showdropped=0;
our $debug=0;
our ($minshow, $maxshow) = (1, 1);
our (@focus, @inclpath, @sysinclpath);
our $filename_regexp;

1;
